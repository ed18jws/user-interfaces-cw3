#include "media_controls.h"

SkipForwardsButton::SkipForwardsButton(ThePlayer* player, qint64 skipSize, ColourScheme colours)
        : PlayerButton(colours), mSkipSizeMS(skipSize), mMediaPlayer(player) {
    setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSeekForward));
    setToolTip("Skip Forwards");
}

void SkipForwardsButton::onButtonPress() {
    mMediaPlayer->skipMedia(mSkipSizeMS);
}

void SkipForwardsButton::onCursorExited() {
}

void SkipForwardsButton::onCursorEntered() {
}

void SkipForwardsButton::onButtonRelease() {
}

SkipBackwardsButton::SkipBackwardsButton(ThePlayer* player, qint64 skipSize, ColourScheme colours)
    : PlayerButton(colours), mSkipSizeMS(skipSize), mMediaPlayer(player) {
    setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSeekBackward));
    setToolTip("Skip Backwards");
}

void SkipBackwardsButton::onButtonPress() {
    mMediaPlayer->skipMedia(-mSkipSizeMS);
}

void SkipBackwardsButton::onCursorExited() {
}

void SkipBackwardsButton::onCursorEntered() {
}

void SkipBackwardsButton::onButtonRelease() {
}
