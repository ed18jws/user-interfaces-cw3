#include "media_controls.h"

PausePlayButton::PausePlayButton(ThePlayer* mediaPlayer, ColourScheme colours)
        : PlayerButton(colours), mMediaPlayer(mediaPlayer) {
    setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
    setToolTip("Pause");
}

void PausePlayButton::onButtonPress() {
    if(mPaused) {
        setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
        setToolTip("Pause");

        mMediaPlayer->play();
        mPaused = false;
    } else {
        setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
        setToolTip("Play");

        mMediaPlayer->pause();
        mPaused = true;
    }
}

void PausePlayButton::onButtonRelease() {
}

void PausePlayButton::onCursorExited() {
}

void PausePlayButton::onCursorEntered() {
}
