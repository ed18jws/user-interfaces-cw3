#include "media_controls.h"
#include "player_window.h"

ToggleLoopButton::ToggleLoopButton(PlayerWindow* window, ThePlayer* player, ColourScheme colours)
        : PlayerButton(colours), mVideoPlayer(player) {

    bool isIconSet = false;

    // Firstly, we need to see if we can locate the custom icon for this button
    QString expectedIconLocation = (window->getVideoDirectory() + "/res/loop_icon.png");
    // We can check whether the icon exists before operating on it
    if(QFile(expectedIconLocation).exists()) {
        QImageReader* imageReader = new QImageReader(expectedIconLocation);
        QImage image = imageReader->read();

        if(!image.isNull()) {
            QIcon* icon = new QIcon(QPixmap::fromImage(image));
            if(icon != nullptr) {
                setIcon(*icon);
                isIconSet = true;
            }
        }
    }

    if(!isIconSet) {
        setText("Loop");
    }

    setToolTip("Loop Videos");
}

void ToggleLoopButton::onButtonPress() {
    const bool newLoopingState = !mLooping;
    mLooping = newLoopingState;

    if(newLoopingState) {
        setToolTip("Stop Looping Videos");
    } else {
        setToolTip("Loop Videos");
    }

    mVideoPlayer->allowLooping(newLoopingState);
}

void ToggleLoopButton::onCursorExited() {
}
void ToggleLoopButton::onCursorEntered() {
}
void ToggleLoopButton::onButtonRelease() {
}
