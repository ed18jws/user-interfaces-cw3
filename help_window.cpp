#include "help_window.h"

HelpWindow::HelpWindow() : QWidget(NULL) {
    setLayout(mLayout);
    setWindowTitle(mWindowTitle);
    setMinimumSize(600, 400);

    // The layout that we use for this window is almost an F-layout, where there is a title row
    // above a large pane of text
    QLabel* titleRowWidget = new QLabel(mHelpHeader);
    titleRowWidget->setFont(mHelpHeaderFont);
    titleRowWidget->setMinimumHeight(35);
    titleRowWidget->setMaximumHeight(35);

    // To accomodate for the window being resized, we must place the text within a scroll area
    QScrollArea* explainationArea = new QScrollArea(this);
    explainationArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    explainationArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    QLabel* mainContentLabel = new QLabel(mHelpContents, explainationArea);
    mainContentLabel->setFont(mHelpContentsFont);
    mainContentLabel->setWordWrap(true);
    explainationArea->setWidget(mainContentLabel);

    mLayout->addWidget(titleRowWidget);
    mLayout->addWidget(explainationArea);

    show();
}

HelpWindow::~HelpWindow() {
    delete mLayout;
}

QString HelpWindow::getMainHelpContents() {
    return QString("Provisional help text. Scroll downwards.")
            .append("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
            .append("Demonstration of text scrolling");
}
