/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <string>
#include <vector>
#include <iostream>

#include <QMessageBox>
#include <QApplication>

#include "player_window.h"
#include "colour_palette.h"

int main(int argc, char* argv[]) {
    // Let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;

    // Create the Qt Application
    QApplication app(argc, argv);

    // If a path wasn't supplied then offer to download the videos
    if(argc != 2) {
        // If we are missing any arguments then we must display an error message - we require a
        // path to our video folder

        // Some horrible string appending happens here to keep the line length to a maximum of 100
        QString errorMessage = "no videos found! download, unzip, and add command line argument to ";
        errorMessage.append("\"quoted\" file location. Download videos from Tom's OneDrive?");

        const int result = QMessageBox::question(nullptr, QString("Tomeo"), errorMessage,
                                                 QMessageBox::Yes | QMessageBox::No);

        // Further horrible string building to keep the line length correct
        QString videoHostString = "https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds";
        videoHostString.append("_ac_uk/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BT");
        videoHostString.append("fdpPag?e=n1qfuN");

        switch(result) {
            case QMessageBox::Yes:
                QDesktopServices::openUrl(QUrl(videoHostString));
                break;
            default:
                break;
        }

        // We exit here because you can't continue without videos to play
        exit(-1);
    }

    PlayerWindow* applicationWindow = new PlayerWindow(QString(argv[1]));
    applicationWindow->setMinimumSize(800, 680);
    applicationWindow->resize(800, 680);
    applicationWindow->show();
    // From the moment the window is shown we need to ensure we hear their keyboard inputs
    applicationWindow->grabKeyboard();

    // Wait for the application to terminate
    return app.exec();
}
