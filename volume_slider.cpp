#include "media_controls.h"

VolumeSlider::VolumeSlider(ThePlayer* player, ColourScheme colours) : QSlider(),
        mColourScheme(colours) {
    setMinimumSize(QSize(150, 25));
    setMaximumSize(QSize(300, 40));
    setOrientation(Qt::Horizontal);
    setStyleSheet(getStyleSheet());

    setRange(0, 100);
    setValue(50);

    player->setVolume(value());

    // Now we need to link the slider's position to the player's volume
    QApplication::connect(this, SIGNAL(valueChanged(int)), player, SLOT(setVolume(int)));
}

QString VolumeSlider::getStyleSheet() {
    QString sheet = "";

    // Set the 'groove' properties
    sheet += "QSlider::groove:horizontal { \n";
    sheet += "  border-radius: 5px; \n";
    sheet += "  background-color: " + mColourScheme.getBackgroundComplimentAsString() + "; \n";
    sheet += "  height: 6px; \n";
    sheet += "} \n";

    // Set the 'handle' properties
    sheet += "QSlider::handle:horizontal { \n";
    sheet += ("  border: 2px solid " + mColourScheme.getForegroundAsString() + "; \n");
    sheet += "  border-radius: 4px; \n";
    sheet += "  background-color: " + mColourScheme.getForegroundAsString() + "; \n";
    sheet += "  width: 8px; \n";
    sheet += "  margin-top: -4px; \n";
    sheet += "  margin-bottom: -4px; \n";
    sheet += "  border-radius: 4px; \n";
    sheet += "} \n";

    sheet += "QSlider::handle:horizontal:hover { \n";
    sheet += ("  border: 2px solid " + mColourScheme.getForegroundAsString() + "; \n");
    sheet += "  border-radius: 4px; \n";
    sheet += "  background-color: " + mColourScheme.getForegroundComplimentAsString() + "; \n";
    sheet += "  width: 8px; \n";
    sheet += "  margin-top: -4px; \n";
    sheet += "  margin-bottom: -4px; \n";
    sheet += "  border-radius: 4px; \n";
    sheet += "} \n";

    return sheet;
}
