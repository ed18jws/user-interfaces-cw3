#include "player_window.h"

PlayerWindow::PlayerWindow(QString directory, ColourScheme colours) : QWidget(NULL),
        mColourScheme(colours), mVideoDirectory(directory) {
    mMediaPlayer = new ThePlayer(this);

    setLayout(mLayout);

    // Make sure we load the videos before constructing the window contents
    updateVideoList();
    constructWindow();

    // Due to an issue where the media player would not appear sometimes (but would reappear after
    // a resize was forced through fullscreen-ing and then reducing the video), the following code
    // exists. Potentially it is due to the player thinking it is fullscreen sometimes; potentially
    // it is some kind of condition that isn't met until the window is scaled up and back down.
    // Regardless, the following two statements appear to fix the problem
    mMediaPlayer->setFullscreen(true);
    mMediaPlayer->setFullscreen(false);
}

PlayerWindow::~PlayerWindow() {
    delete mLayout;
    delete mCurrentHelpWindow;
    delete mVideoSelectButtons;
    delete mAllVideoInformation;
}

void PlayerWindow::updateVideoList() {
    // Firstly, we must begin by clearing away all of the existing video information
    if(mAllVideoInformation != nullptr) {
        delete mAllVideoInformation;
    }

    // Now we must load the videos from the specified directory
    mAllVideoInformation = new std::vector<TheButtonInfo>();
    QDir videoDirectory(mVideoDirectory);
    QDirIterator directoryIterator(videoDirectory);

    while(directoryIterator.hasNext()) {
        QString currentFile = directoryIterator.next();
#if defined(_WIN32)
        // Windows' supported video file-format
        if (currentFile.contains(".wmv"))  {
#else
        // Mac & Linux supported video file-format
        if (currentFile.contains(".mp4") || currentFile.contains("MOV"))  {
#endif
            // If we are here, we've identified that this is a video file, so also find the
            // thumbnail (should be the same name with a PNG extension)
            QString thumbnail = currentFile.left(currentFile.length() - 4) + ".png";

            // We've worked out what the thumbnail name should be, now see if it exists
            if(QFile(thumbnail).exists()) {
                QImageReader* imageReader = new QImageReader(thumbnail);
                QImage sprite = imageReader->read();

                if(!sprite.isNull()) {
                    QIcon* icon = new QIcon(QPixmap::fromImage(sprite));
                    QUrl* url = new QUrl(QUrl::fromLocalFile(currentFile));

                    // Add our button information to our results list
                    mAllVideoInformation->push_back(TheButtonInfo(url, icon));
                } else {
                    qDebug() << "warning: skipping video because I couldn't process thumbnail "
                             << thumbnail << Qt::endl;
                }
            } else {
                qDebug() << "warning: skipping video because I couldn't find thumbnail "
                         << thumbnail << Qt::endl;
            }
        }
    }

    // Now we've updated the videoes available, reload the selection buttons
    updateVideoSelectionButtons();
}

void PlayerWindow::updateVideoSelectionButtons() {
    // If we have a widget already then we need to clear all the buttons already in there
    if(mVideoSelectionWidget != nullptr) {
        // While there are items in the widget's layout, remove the items
        while(mVideoSelectionWidget->layout()->count() > 0) {
            // TakeAt is a destructive peek (meaning it retrieves while removing from the list)
            QLayoutItem* currentItem = mVideoSelectionWidget->layout()->takeAt(0);
            delete currentItem;
        }
    } else {
        // If we don't already have the widget initialised then set it up (including the layout)
        mVideoSelectionWidget = new QWidget();
        QHBoxLayout* buttonWidgetLayout = new QHBoxLayout();
        mVideoSelectionWidget->setLayout(buttonWidgetLayout);
    }

    // Now we can create the individual buttons and place them within the widget
    mVideoSelectButtons = new std::vector<TheButton*>();
    if(mAllVideoInformation != nullptr) {
        for(unsigned i = 0; i < mAllVideoInformation->size(); i++) {
            TheButton* currentSelectionButton = new TheButton(mVideoSelectionWidget);
            currentSelectionButton->init(&(mAllVideoInformation->at(i)));
            // When clicked, tell the player to play the new video for this button
            currentSelectionButton->connect(currentSelectionButton, SIGNAL(jumpTo(TheButtonInfo*)),
                                            mMediaPlayer, SLOT(jumpTo(TheButtonInfo*)));

            mVideoSelectionWidget->layout()->addWidget(currentSelectionButton);

            mVideoSelectButtons->push_back(currentSelectionButton);
        }
    }
}

void PlayerWindow::keyPressEvent(QKeyEvent* event) {
    const int keycode = event->key();

    if(keycode == Qt::Key_F1) {
        constructHelpWindow();
    } else if(keycode == Qt::Key_Escape) {
        // When the escape key is pressed, we must check whether the media player is currently
        // fullscreen (and if so, we can then exit fullscreen mode)
        if(mMediaPlayer->isFullscreen()) {
            mMediaPlayer->setFullscreen(false);
        }
    } else if(keycode == Qt::Key_Left) {
        const qint64 msTimeStep = -5000;
        mMediaPlayer->skipMedia(msTimeStep);
    } else if(keycode == Qt::Key_Right) {
        const qint64 msTimeStep = 5000;
        mMediaPlayer->skipMedia(msTimeStep);
    }
}

void PlayerWindow::keyReleaseEvent(QKeyEvent* event) {
    // We actually don't require this method to do anything, however, for completeness, we've
    // inherited it from this class' parent and given a basic outline for how it would work
    const int keycode = event->key();

    switch(keycode) {
        default:
            break;
    }
}

void PlayerWindow::constructHelpWindow() {
    // The issue with opening the help window is that when the user closes it, the memory is not
    // cleaned automatically. Since we only recieve close events on widgets while they are being
    // processed, it isn't safe to clear the memory there (until Qt has stopped using it in it's
    // event distributor).
    // The work-around is to always contain a pointer here and let that memory become wasted once
    // the help window is closed (but to always make sure we never allocate out more then one
    // section of memory to prevent a memory leak)

    if(mCurrentHelpWindow != NULL) {
        // When a widget is closed it is said to no longer be visible (this is different to a
        // widget being obscured by another widget in the same layout because the HelpWindow
        // isn't in a parent widget with other elements)
        if(mCurrentHelpWindow->isVisible()) {
            mCurrentHelpWindow->close();
            delete mCurrentHelpWindow;
            mCurrentHelpWindow = NULL;
        }
    }

    // Now we've established a state where the current help window should always be NULL at this
    // point. As such, we can now create a new help window (which implicitly makes itself shown)
    mCurrentHelpWindow = new HelpWindow();
}

void PlayerWindow::constructWindow() {
    setStyleSheet("background-color: " + mColourScheme.getBackgroundAsString() + ";");

    // Now we can work on making the video selection buttons
    // We will begin by having a scrollable area to contain all of the buttons
    QScrollArea* buttonsArea = new QScrollArea();
    buttonsArea->setWidgetResizable(true);
    buttonsArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    buttonsArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    // If a size smaller than this was used, Qt will overlap the scroll bar over the video widgets
    buttonsArea->setMinimumHeight(150);

    buttonsArea->setWidget(mVideoSelectionWidget);

    // If we have at least one video, pre-load it
    if(mAllVideoInformation != nullptr) {
        if(mAllVideoInformation->size() > 0) {
            mMediaPlayer->setContent(&(mAllVideoInformation->at(0)));
        }
    }

    mLayout->addWidget(buttonsArea);
    mLayout->addWidget(mMediaPlayer->getVideoWidget());
    mLayout->addWidget(new MediaControlsWidget(this, mMediaPlayer, mColourScheme));

    // We force a repaint because we were having intermittent issues with widgets not appearing
    repaint();
}

QString PlayerWindow::getVideoDirectory() {
    return mVideoDirectory;
}




























