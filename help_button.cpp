#include "media_controls.h"
// We must include player_window here and not in the media_controls header because it would lead
// to recursive inclusion between the two header files
#include "player_window.h"

HelpButton::HelpButton(PlayerWindow* window, ColourScheme colours) : PlayerButton(colours),
        mWindow(window) {
    setIcon(QApplication::style()->standardIcon(QStyle::SP_FileDialogInfoView));
    setToolTip("Help");
}

void HelpButton::onButtonPress() {
    mWindow->constructHelpWindow();
}

void HelpButton::onCursorExited() {
}

void HelpButton::onCursorEntered() {
}

void HelpButton::onButtonRelease() {
}
