#include "media_controls.h"
#include "player_window.h"

FullScreenButton::FullScreenButton(PlayerWindow* window, ThePlayer* player, ColourScheme colours)
        : PlayerButton(colours), mMediaPlayer(player) {

    bool isIconSet = false;

    // Firstly, we need to see if we can locate the custom icon for this button
    QString expectedIconLocation = (window->getVideoDirectory() + "/res/fullscreen_icon.png");
    // We can check whether the icon exists before operating on it
    if(QFile(expectedIconLocation).exists()) {
        QImageReader* imageReader = new QImageReader(expectedIconLocation);
        QImage image = imageReader->read();

        if(!image.isNull()) {
            QIcon* icon = new QIcon(QPixmap::fromImage(image));
            if(icon != nullptr) {
                setIcon(*icon);
                isIconSet = true;
            }
        }
    }

    if(!isIconSet) {
        setText("Fullscreen");
    }

    setToolTip("Toggle Fullscreen");
}

void FullScreenButton::onButtonPress() {
    mMediaPlayer->toggleFullscreen();
}

void FullScreenButton::onCursorExited() {
}

void FullScreenButton::onCursorEntered() {
}

void FullScreenButton::onButtonRelease() {
}
