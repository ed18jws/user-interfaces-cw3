#include "the_button.h"

TheButtonInfo::TheButtonInfo(QUrl* url, QIcon* icon) : url(url), icon(icon) {
}

TheButton::TheButton(QWidget* parent) : QPushButton(parent) {
     setIconSize(QSize(200, 110));

     // Call 'clicked' whenever the button is pressed and then subsequently released
     connect(this, SIGNAL(released()), this, SLOT(clicked()));
}

void TheButton::init(TheButtonInfo* i) {
    setIcon(*(i->icon));
    info = i;
}

void TheButton::clicked() {
    emit jumpTo(info);
}
