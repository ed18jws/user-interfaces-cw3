#ifndef CW2_PLAYER_BUTTON_H
#define CW2_PLAYER_BUTTON_H

#include <QEvent>
#include <QStyle>
#include <QWidget>
#include <QVariant>
#include <QPushButton>
#include <QResizeEvent>

#include "colour_palette.h"

class PlayerButton : public QPushButton {
    // Declare Qt's macro for signals and slots
    Q_OBJECT

    private:
        ColourScheme mColourScheme;

        bool mHovered = false;
        bool mPressed = false;

    public:
        PlayerButton(ColourScheme colourScheme = ColourScheme(), QWidget* parent = nullptr);
        ~PlayerButton();

    protected:
        // This is an inherited method from QWidget that recieves QEvents and returns a boolean
        // indicating whether the event has been absorbed by this object (with the alternative
        // being that the event is passed up the inheritence tree to the parent)
        bool event(QEvent* event);

        virtual void onButtonPress() = 0;
        virtual void onCursorExited() = 0;
        virtual void onCursorEntered() = 0;
        virtual void onButtonRelease() = 0;

    private:
        void internalOnHoverExit();
        void internalOnHoverEnter();

        QString getStyleSheet();

    private slots:
        void slotOnButtonPress();
        void slotOnButtonRelease();
};

#endif // CW2_PLAYER_BUTTON_H
