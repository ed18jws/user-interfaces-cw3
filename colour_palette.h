#ifndef CW2_COLOUR_PALETTE_H
#define CW2_COLOUR_PALETTE_H

#include <string>

#include <QColor>

// This class encapsulates five colours (stored in hexadecimal format)
class ColourScheme {
    public:
        const QColor accentColour;
        const QColor backgroundColour;
        const QColor foregroundColour;
        const QColor backgroundComplimentColour;
        const QColor foregroundComplimentColour;

    public:
        // This constructor takes in the colours for this colour scheme in hexadecimal format,
        // where the data structure is a 32-bit integer (with the first 8 bits being unused,
        // followed by 8 bits for the red channel, followed by 8 bits for the green channel,
        // finished with 8 bits for the blue channel
        ColourScheme(QColor accent = QColor(220, 170, 111),
                     QColor background = QColor(68, 81, 83),
                     QColor foreground = QColor(200, 216, 156),
                     QColor backgroundCompliment = QColor(152, 179, 164),
                     QColor foregroundCompliment = QColor(226, 243, 224))
                : accentColour(accent), backgroundColour(background), foregroundColour(foreground),
                backgroundComplimentColour(backgroundCompliment),
                foregroundComplimentColour(foregroundCompliment) {
            // Don't need to do anything here
        }

        inline QString getAccentAsString() const {
            return getColourAsRGBString(accentColour);
        }

        inline QString getBackgroundAsString() const {
            return getColourAsRGBString(backgroundColour);
        }

        inline QString getForegroundAsString() const {
            return getColourAsRGBString(foregroundColour);
        }

        inline QString getBackgroundComplimentAsString() const {
            return getColourAsRGBString(backgroundComplimentColour);
        }

        inline QString getForegroundComplimentAsString() const {
            return getColourAsRGBString(foregroundComplimentColour);
        }

    private:
        inline QString getColourAsRGBString(QColor color) const {
            int r = 0, g = 0, b = 0;
            color.getRgb(&r, &g, &b);
            return QString("rgb(").append(std::to_string(r).c_str()).append(", ")
                    .append(std::to_string(g).c_str()).append(", ")
                    .append(std::to_string(b).c_str()).append(")");
        }
};

#endif // CW2_COLOUR_PALETTE_H
