#include "media_controls.h"

MuteButton::MuteButton(ThePlayer* player, ColourScheme colours) : PlayerButton(colours),
        mMediaPlayer(player) {
    // We need to start with the video muted, so make sure the button reflects that
    setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaVolumeMuted));
    setToolTip("Unmute Audio");
    mMediaPlayer->setMuted(true);
}

void MuteButton::onButtonPress() {
    // We need to toggle the mute status whenever the button is pressed
    const bool nextMuteStatus = !mMediaPlayer->isMuted();
    mMediaPlayer->setMuted(nextMuteStatus);

    if(nextMuteStatus) {
        setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaVolumeMuted));
        setToolTip("Unmute Audio");
    } else {
        setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaVolume));
        setToolTip("Mute Audio");
    }
}

void MuteButton::onCursorExited() {
}

void MuteButton::onCursorEntered() {
}

void MuteButton::onButtonRelease() {
}
