#ifndef CW2_MEDIA_CONTROLS_H
#define CW2_MEDIA_CONTROLS_H

#include <cstdio>
#include <vector>
#include <iostream>
#include <filesystem>

#include <QDir>
#include <QStyle>
#include <QSlider>
#include <QWidget>
#include <QFileInfo>
#include <QFileDialog>
#include <QPushButton>
#include <QSizePolicy>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QDirIterator>
#include <QImageReader>
#include <QDesktopServices>

#include "the_player.h"
#include "player_button.h"
#include "colour_palette.h"

// We forward declare the player window instead of including because it in-turn will then include
// this header file
class PlayerWindow;

class PausePlayButton : public PlayerButton {
    private:
        bool mPaused = false;
        ThePlayer* mMediaPlayer = nullptr;

   public:
        PausePlayButton(ThePlayer* mediaPlayer, ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class StopButton : public PlayerButton {
    private:
        ThePlayer* mMediaPlayer = nullptr;

    public:
        StopButton(ThePlayer* player, ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class FullScreenButton : public PlayerButton {
    private:
        ThePlayer* mMediaPlayer = nullptr;

    public:
        FullScreenButton(PlayerWindow* window, ThePlayer* player,
                         ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class MuteButton : public PlayerButton {
    private:
        ThePlayer* mMediaPlayer = nullptr;

    public:
        MuteButton(ThePlayer* player, ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class SkipForwardsButton : public PlayerButton {
    private:
        qint64 mSkipSizeMS = 0;
        ThePlayer* mMediaPlayer = nullptr;

    public:
        SkipForwardsButton(ThePlayer* player, qint64 skipSize = 5000,
                           ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class SkipBackwardsButton : public PlayerButton {
    private:
        qint64 mSkipSizeMS = 0;
        ThePlayer* mMediaPlayer = nullptr;

    public:
        SkipBackwardsButton(ThePlayer* player, qint64 skipSize = 5000,
                            ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class HelpButton : public PlayerButton {
    private:
        PlayerWindow* mWindow;

    public:
        HelpButton(PlayerWindow* window, ColourScheme colours);

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class UploadButton : public PlayerButton {
    private:
        ThePlayer* mThePlayer;
        PlayerWindow* mPlayerWindow;

    public:
        UploadButton(PlayerWindow* window, ThePlayer* thePlayer, ColourScheme colours);

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class ToggleLoopButton : public PlayerButton {
    private:
        bool mLooping = false;
        ThePlayer* mVideoPlayer = nullptr;

   public:
        ToggleLoopButton(PlayerWindow* window, ThePlayer* player,
                         ColourScheme colours = ColourScheme());

        void onButtonPress();
        void onCursorExited();
        void onCursorEntered();
        void onButtonRelease();
};

class VolumeSlider : public QSlider {
    private:
        ColourScheme mColourScheme = ColourScheme();

    public:
        VolumeSlider(ThePlayer* player, ColourScheme colours = ColourScheme());

    private:
        QString getStyleSheet();
};

// This class is the main parent widget that will contain all of the media buttons (such as pause
// and play) but not the volume controls
class GeneralMediaControlsWidget : public QWidget {
    private:
        PlayerWindow* mWindow = nullptr;
        ThePlayer* mThePlayer = nullptr;

        QHBoxLayout* mLayout = new QHBoxLayout();
        ColourScheme mColourScheme = ColourScheme();

        HelpButton* mHelpButton = nullptr;
        StopButton* mStopButton = nullptr;
        UploadButton* mUploadButton = nullptr;
        PausePlayButton* mPausePlayButton = nullptr;
        FullScreenButton* mFullscreenButton = nullptr;
        ToggleLoopButton* mToggleLoopButton = nullptr;
        SkipForwardsButton* mSkipForwardsButton = nullptr;
        SkipBackwardsButton* mSkipBackwardsButton = nullptr;

    public:
        GeneralMediaControlsWidget(PlayerWindow* window, ThePlayer* thePlayer,
                                   ColourScheme colours = ColourScheme(), QWidget* parent = 0);
        ~GeneralMediaControlsWidget();
};

class VolumeMediaControlsWidget : public QWidget {
    private:
        ThePlayer* mThePlayer = nullptr;

        QHBoxLayout* mLayout = new QHBoxLayout();
        ColourScheme mColourScheme = ColourScheme();

        MuteButton* mMuteButton = nullptr;
        VolumeSlider* mVolumeSlider = nullptr;

    public:
        VolumeMediaControlsWidget(ThePlayer* thePlayer, ColourScheme colours, QWidget* parent);
        ~VolumeMediaControlsWidget();
};

class MediaControlsWidget : public QWidget {
    private:
        PlayerWindow* mWindow = nullptr;
        ThePlayer* mThePlayer = nullptr;

        QVBoxLayout* mLayout = new QVBoxLayout();
        ColourScheme mColourScheme = ColourScheme();

        VolumeMediaControlsWidget* mVolumeWidget = nullptr;
        GeneralMediaControlsWidget* mButtonsWidget = nullptr;

    public:
        MediaControlsWidget(PlayerWindow* window, ThePlayer* thePlayer,
                            ColourScheme colours = ColourScheme(), QWidget* parent = 0);
        ~MediaControlsWidget();
};

#endif // CW2_MEDIA_CONTROLS_H
