#ifndef CW2_PLAYER_WINDOW_H
#define CW2_PLAYER_WINDOW_H

#include <QDir>
#include <QLayout>
#include <QSlider>
#include <QWidget>
#include <QKeyEvent>
#include <QFileInfo>
#include <QLayoutItem>
#include <QScrollArea>
#include <QToolButton>
#include <QVBoxLayout>
#include <QVideoWidget>
#include <QDirIterator>
#include <QImageReader>
#include <QDesktopServices>

#include "the_button.h"
#include "the_player.h"
#include "help_window.h"
#include "media_controls.h"
#include "colour_palette.h"

class PlayerWindow : public QWidget {
    private:
        ColourScheme mColourScheme = ColourScheme();
        ThePlayer* mMediaPlayer = nullptr;

        QWidget* mVideoSelectionWidget = nullptr;
        QVBoxLayout* mLayout = new QVBoxLayout();

        // This directory path should not have a trailing '/' (e.g. "videos" instead of "videos/")
        QString mVideoDirectory;
        std::vector<TheButton*>* mVideoSelectButtons;
        std::vector<TheButtonInfo>* mAllVideoInformation = nullptr;

        HelpWindow* mCurrentHelpWindow = nullptr;

    public:
        PlayerWindow(QString directory, ColourScheme colours = ColourScheme());
        ~PlayerWindow();

        void updateVideoList();

        void constructHelpWindow();

        QString getVideoDirectory();

    protected:
        void keyPressEvent(QKeyEvent* event);
        void keyReleaseEvent(QKeyEvent* event);

    private:
        void constructWindow();

        void updateVideoSelectionButtons();
};

#endif // CW2_PLAYER_WINDOW_H
