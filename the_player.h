#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H

#include <vector>

#include <QTimer>
#include <QToolButton>
#include <QApplication>
#include <QMediaPlayer>
#include <QVideoWidget>

#include "the_button.h"

class ThePlayer : public QMediaPlayer {
    // Declare Qt's macro for signals and slots
    Q_OBJECT

    private:
        QWidget* mWindowWidget = nullptr;
        QVideoWidget* mVideoWidget = nullptr;

        bool mLooping = false;

    public:
        ThePlayer(QWidget* windowWidget, QWidget* parent = nullptr);
        ~ThePlayer();

        void setContent(TheButtonInfo* videoDetails);
        void allowLooping(bool condition);

        void toggleFullscreen();
        void setFullscreen(bool makeFullscreen);

        void skipMedia(const qint64& msSkipSize);

        bool isFullscreen();
        QWidget* getWindowWidget();
        QVideoWidget* getVideoWidget();

    private slots:
        void playStateChanged(QMediaPlayer::State mediaState);

    public slots:
        // Start playing the media from the given button information
        void jumpTo(TheButtonInfo* videoDetails);
};

#endif //CW2_THE_PLAYER_H
