#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H

#include <QUrl>
#include <QPushButton>

class TheButtonInfo {
    public:
        // URL of the file to play
        QUrl* url;
        // Button icon
        QIcon* icon;

    public:
        TheButtonInfo(QUrl* url, QIcon* icon);
};

class TheButton : public QPushButton {
    // Declare Qt's macro for signals and slots
    Q_OBJECT

    public:
        // This structure stores the core data for the button
        TheButtonInfo* info;

        TheButton(QWidget* parent = nullptr);

        void init(TheButtonInfo* i);

    private slots:
        void clicked();

    signals:
        void jumpTo(TheButtonInfo*);

};

#endif //CW2_THE_BUTTON_H
