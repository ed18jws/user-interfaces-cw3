#ifndef CW2_HELP_WINDOW_H
#define CW2_HELP_WINDOW_H

#include <QLabel>
#include <QString>
#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>

class HelpWindow : public QWidget {
    private:
        QVBoxLayout* mLayout = new QVBoxLayout();
        const QString mWindowTitle = QString("Tomeo Help");

        const QString mHelpContents = getMainHelpContents();
        const QString mHelpHeader = QString("How To Use Tomeo:");
        const QFont mHelpHeaderFont = QFont("Arial", 24, QFont::Bold);
        const QFont mHelpContentsFont = QFont("Arial", 16, QFont::Normal);

    public:
        HelpWindow();
        ~HelpWindow();

    private:
        QString getMainHelpContents();
};

#endif // CW2_HELP_WINDOW_H
