#include "media_controls.h"

StopButton::StopButton(ThePlayer* player, ColourScheme colours) : PlayerButton(colours),
        mMediaPlayer(player) {
    setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaStop));
    setToolTip("Stop");
}

void StopButton::onButtonPress() {
    mMediaPlayer->stop();
}

void StopButton::onCursorExited() {
}

void StopButton::onCursorEntered() {
}

void StopButton::onButtonRelease() {
}
