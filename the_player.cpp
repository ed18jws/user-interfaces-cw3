#include "the_player.h"

ThePlayer::ThePlayer(QWidget* windowWidget, QWidget* parent) : QMediaPlayer(parent),
    mWindowWidget(windowWidget) {

    // We need the video output for our media player to be constructed
    mVideoWidget = new QVideoWidget(parent);
    mVideoWidget->setMinimumSize(QSize(320, 180));
    setVideoOutput(mVideoWidget);

    // Call 'playStateChanged' whenever the video starts/stops/pauses
    connect(this, SIGNAL(stateChanged(QMediaPlayer::State)), this,
            SLOT(playStateChanged(QMediaPlayer::State)));
}

ThePlayer::~ThePlayer() {
    // The buttons, button info and window widgets are references to things external to this class.
    // It's important we don't delete them (incase we ever destroy a player and need the window etc
    // to be intact). As such we only delete the video widget here
    delete mVideoWidget;
}

void ThePlayer::setContent(TheButtonInfo* video) {
    // We don't do much here, just try and play whatever we've been given
    jumpTo(video);
}

void ThePlayer::allowLooping(bool condition) {
    mLooping = condition;
}

void ThePlayer::skipMedia(const qint64& msSkipSize) {
    const qint64 minimumMediaPosition = 0;
    const qint64 maximumMediaPosition = duration();

    qint64 nextMediaPosition = (position() + msSkipSize);

    if(nextMediaPosition > maximumMediaPosition) {
        nextMediaPosition = maximumMediaPosition;
    }

    if(nextMediaPosition < minimumMediaPosition) {
        nextMediaPosition = minimumMediaPosition;
    }

    setPosition(nextMediaPosition);
}

void ThePlayer::toggleFullscreen() {
    const bool currentFullscreenState = isFullscreen();
    setFullscreen(!currentFullscreenState);
}

void ThePlayer::setFullscreen(bool makeFullscreen) {
    mVideoWidget->setFullScreen(makeFullscreen);

    // When we enter fullscreen mode, the video widget takes the keyboard controls (so the key
    // inputs aren't captured by the window class). To avoid this we release the control from this
    // widget and give it to the window
    mVideoWidget->releaseKeyboard();
    mWindowWidget->grabKeyboard();
}

bool ThePlayer::isFullscreen() {
    return (mVideoWidget->isFullScreen());
}

QVideoWidget* ThePlayer::getVideoWidget() {
    return mVideoWidget;
}

void ThePlayer::playStateChanged(QMediaPlayer::State mediaState) {
    switch(mediaState) {
        case QMediaPlayer::State::StoppedState:
            if(mLooping) {
                play();
            }
            break;
        default:
            break;
    }
}

void ThePlayer::jumpTo(TheButtonInfo* videoDetails) {
    // We call this method whenever we switch between videos, but we must keep volume controls
    // consistent between videos, so store the previous values and copy them across
    const int previousVolume = volume();
    const bool previouslyMutedStatus = isMuted();

    if(videoDetails != nullptr) {
        setMedia(*(videoDetails->url));
    }

    setVolume(previousVolume);
    setMuted(previouslyMutedStatus);

    play();

    if(mWindowWidget != nullptr) {
        mWindowWidget->setWindowTitle("Tomeo - " + videoDetails->url->fileName());
    }
}
