QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        full_screen_button.cpp \
        help_button.cpp \
        help_window.cpp \
        media_controls.cpp \
        mute_button.cpp \
        pause_play_button.cpp \
        player_button.cpp \
        player_window.cpp \
        stop_button.cpp \
        the_button.cpp \
        the_player.cpp \
        position_skipping_buttons.cpp \
        toggle_loop_button.cpp \
        upload_button.cpp \
        tomeo.cpp \
        volume_slider.cpp

HEADERS += \
    colour_palette.h \
    help_window.h \
    media_controls.h \
    player_button.h \
    player_window.h \
    the_button.h \
    the_player.h


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

