#include "player_button.h"

PlayerButton::PlayerButton(ColourScheme colourScheme, QWidget* parent) : QPushButton(parent),
        mColourScheme(colourScheme) {
    // We must provide the CSS-format stylesheet for the button to use
    setStyleSheet(getStyleSheet());

    // We need to define the limits of the button's size. These sizes are semi-arbitrary, but yield
    // a nice layout in the layout at the time of writing this class
    setMinimumSize(QSize(24, 24));
    setMaximumSize(QSize(48, 48));

    // Currently the buttons do not scale at a 1:1 resolution (or any guaranteed resolution), so
    // currently the size must be fixed
    //TODO: Size should vary between 24 and 48, while maintaining a 1:1 ratio
    setFixedSize(QSize(32, 32));


    // Now we need to connect our input events (like being pressed) to our methods
    connect(this, SIGNAL(pressed()), this, SLOT(slotOnButtonPress()));
    connect(this, SIGNAL(released()), this, SLOT(slotOnButtonRelease()));
}

PlayerButton::~PlayerButton() {
}

bool PlayerButton::event(QEvent* event) {
    if(event->type() == QEvent::HoverEnter) {
        internalOnHoverEnter();
        return true;
    } else if(event->type() == QEvent::HoverLeave) {
        internalOnHoverExit();
        return true;
    }

    return QPushButton::event(event);
}

void PlayerButton::slotOnButtonPress() {
    mPressed = true;
    setStyleSheet(getStyleSheet());
    onButtonPress();
}

void PlayerButton::internalOnHoverExit() {
    mHovered = false;
    setStyleSheet(getStyleSheet());
    onCursorExited();
}

void PlayerButton::internalOnHoverEnter() {
    mHovered = true;
    setStyleSheet(getStyleSheet());
    onCursorEntered();
}

void PlayerButton::slotOnButtonRelease() {
    mPressed = false;
    setStyleSheet(getStyleSheet());
    onButtonRelease();
}

QString PlayerButton::getStyleSheet() {
    QString sheet = "";
    sheet += "border-radius: 6%; \n";
    sheet += "text-align: center; \n";

    if(mPressed) {
        sheet += ("border: 2px solid " + mColourScheme.getAccentAsString() + ";");
    } else {
        sheet += ("border: 2px solid " + mColourScheme.getForegroundAsString() + ";");
    }

    if(mHovered) {
        sheet += ("background-color: " + mColourScheme.getForegroundComplimentAsString() + "; \n");
    } else {
        sheet += ("background-color: " + mColourScheme.getForegroundAsString() + "; \n");
    }

    return sheet;
}
