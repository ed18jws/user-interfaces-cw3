#include "media_controls.h"
// Must include player window here and not in the header because they reference each other so there
// would be a recursive inclusion
#include "player_window.h"

static void uploadSingleFileToDirectory(QString message, QString targetDirectory);

UploadButton::UploadButton(PlayerWindow* window, ThePlayer* thePlayer, ColourScheme colours)
        : PlayerButton(colours), mThePlayer(thePlayer), mPlayerWindow(window) {
    setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowUp));
    setToolTip("Upload Video");
}

void UploadButton::onButtonPress() {
    QString tomeoDir = mPlayerWindow->getVideoDirectory();

    // Each video file also requires a thumbnail, so make sure that we get the user to supply one
    // of those too
    uploadSingleFileToDirectory(QString("Please select your video file..."), tomeoDir);
    uploadSingleFileToDirectory(QString("Please select your video's thumbnail..."), tomeoDir);

    // Now we've potentially updated the available videos, so refresh the content in the media
    // player
    mPlayerWindow->updateVideoList();
}

void UploadButton::onCursorExited() {
}

void UploadButton::onCursorEntered() {
}

void UploadButton::onButtonRelease() {
}

static void uploadSingleFileToDirectory(QString message, QString targetDirectory) {    
    // We need to open a file dialog whenever the button is pressed
    QString fileURL = QFileDialog::getOpenFileName(NULL, message,
                                                   QString(targetDirectory));

    // Get whatever is after the last '/' in the URL (e.g. for "tomeo/test/vids/asd.x.y" you would
    // get "asd.x.y" back)
    QString fileName = fileURL.mid(fileURL.lastIndexOf("/"));

    // Now we need to work out where this file is going to be uploaded to (including the file name
    // and it's extensions)
    QString newFileURL = QString(targetDirectory);
    newFileURL.append("/").append(fileName);

    // Now move the file!
    std::rename(QByteArray(fileURL.toLocal8Bit().data()),
                QByteArray(newFileURL.toLocal8Bit().data()));
}
