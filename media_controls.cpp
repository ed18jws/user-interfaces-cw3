#include "media_controls.h"

GeneralMediaControlsWidget::GeneralMediaControlsWidget(PlayerWindow* window, ThePlayer* thePlayer,
                                                       ColourScheme colours, QWidget* parent)
        : QWidget(parent), mWindow(window), mThePlayer(thePlayer), mColourScheme(colours) {
    // We are assuming the spacing unit for the minimum size is 2 and the minimum button size is 48
    setMaximumSize(QSize((24 + 2) * 8, 2 + 24 + 2));
    // We are assuming the spacing unit for the maximum size is 4 and the maximum button size is 48
    setMaximumSize(QSize((48 + 4) * 8, 4 + 48 + 4));

    mLayout->setMargin(0);
    mLayout->setAlignment(Qt::AlignCenter);
    setLayout(mLayout);

    // Now we must define the individual buttons in the media controls
    mStopButton = new StopButton(thePlayer, colours);
    mPausePlayButton = new PausePlayButton(thePlayer, colours);
    mFullscreenButton = new FullScreenButton(window, thePlayer, colours);
    mToggleLoopButton = new ToggleLoopButton(window, thePlayer, colours);
    mSkipForwardsButton = new SkipForwardsButton(thePlayer, 5000, colours);
    mSkipBackwardsButton = new SkipBackwardsButton(thePlayer, 5000, colours);
    mHelpButton = new HelpButton(window, colours);
    mUploadButton = new UploadButton(window, thePlayer, colours);

    // Now we simply add the buttons in to the single composite widget
    mLayout->addWidget(mSkipBackwardsButton);
    mLayout->addWidget(mPausePlayButton);
    mLayout->addWidget(mStopButton);
    mLayout->addWidget(mSkipForwardsButton);

    mLayout->addWidget(mFullscreenButton);
    mLayout->addWidget(mToggleLoopButton);

    mLayout->addWidget(mUploadButton);
    mLayout->addWidget(mHelpButton);
}

GeneralMediaControlsWidget::~GeneralMediaControlsWidget() {
}

VolumeMediaControlsWidget::VolumeMediaControlsWidget(ThePlayer* thePlayer, ColourScheme colours,
                                                     QWidget* parent)
        : QWidget(parent), mThePlayer(thePlayer), mColourScheme(colours) {
    // We are assuming the spacing unit for the minimum size is 2 and the minimum button size is 48
    setMaximumSize(QSize(150 + 2 + 24, 2 + 24 + 2));
    // We are assuming the spacing unit for the maximum size is 4 and the maximum button size is 48
    setMaximumSize(QSize(300 + 4 + 48, 4 + 48 + 4));

    mLayout->setMargin(0);
    mLayout->setAlignment(Qt::AlignCenter);
    setLayout(mLayout);

    // Now we must define the individual buttons in the media controls
    mMuteButton = new MuteButton(thePlayer, colours);
    mVolumeSlider = new VolumeSlider(thePlayer, colours);

    // Now we simply add the buttons in to the single composite widget
    mLayout->addWidget(mVolumeSlider);
    mLayout->addWidget(mMuteButton);
}

VolumeMediaControlsWidget::~VolumeMediaControlsWidget() {
}

MediaControlsWidget::MediaControlsWidget(PlayerWindow* window, ThePlayer* thePlayer,
                                         ColourScheme colours, QWidget* parent)
        : QWidget(parent), mWindow(window), mThePlayer(thePlayer), mColourScheme(colours) {
    mLayout->setMargin(0);
    setLayout(mLayout);
    mLayout->setAlignment(Qt::AlignCenter);

    mVolumeWidget = new VolumeMediaControlsWidget(thePlayer, colours, parent);
    mButtonsWidget = new GeneralMediaControlsWidget(window, thePlayer, colours, parent);

    mLayout->addWidget(mButtonsWidget);
    mLayout->addWidget(mVolumeWidget);
}

MediaControlsWidget::~MediaControlsWidget() {
}
